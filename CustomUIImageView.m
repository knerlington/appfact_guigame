//
//  CustomUIImageView.m
//  guiGame
//
//  Created by Dan Lakss on 06/10/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "CustomUIImageView.h"
@interface CustomUIImageView ()
@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;
-(void)didTap;
@end

@implementation CustomUIImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(instancetype)init{
    self = [super init];
    if(self){
		self.tapRecognizer = [[UITapGestureRecognizer alloc]init];
		self.userInteractionEnabled = YES;
		[self addGestureRecognizer:self.tapRecognizer];
		[self.tapRecognizer addTarget:self action:@selector(didTap)];
		
    }
    return self;
}
-(void)didTap{
	//did tap self.gestureRecognizer
	if([self.delegate respondsToSelector:@selector(didTap)]){
		[self.delegate didTap];
	}else{
		NSLog(@"CustomUIImageView: Sorry no delegate for custom imageView responds to tap recognizer.");
	}
	
}
@end
