//
//  CustomUIImageView.h
//  guiGame
//
//  Created by Dan Lakss on 06/10/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GestureDelegate <NSObject>

-(void)didTap;

@end

@interface CustomUIImageView : UIImageView
-(instancetype)init;
@property (nonatomic, strong) UITapGestureRecognizer *tap;
@property (nonatomic, strong) id<GestureDelegate> delegate;

@end
