//
//  ViewControllerDelegate.h
//  guiGame
//
//  Created by Dan Lakss on 2015-09-30.
//  Copyright © 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ViewControllerDelegate <NSObject>
-(BOOL)shouldSegue;


@end
