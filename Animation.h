//
//  Animation.h
//  guiGame
//
//  Created by Dan Lakss on 06/10/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef enum{
    ROTATE = 0,
    MOVE = 1
}ANIMATION_TYPE;


@interface Animation : NSObject
@property (nonatomic, strong) NSMutableArray *coordinateList;
@property (nonatomic, strong) NSString *animationTypeFromDictionary;
@property (nonatomic) ANIMATION_TYPE animationType;
@property (nonatomic) BOOL repeat;
-(instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
