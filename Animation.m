//
//  Animation.m
//  guiGame
//
//  Created by Dan Lakss on 06/10/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "Animation.h"

@implementation Animation
-(instancetype)initWithDictionary:(NSDictionary*)dictionary{
    self = [super init];
    if(self){
        _coordinateList = [[NSMutableArray alloc]init];
        NSArray *coords = [dictionary objectForKey:@"animationCoords"];
        _coordinateList = [NSMutableArray arrayWithArray:coords];
        _animationTypeFromDictionary = [dictionary objectForKey:@"animationType"];
        _repeat = [dictionary objectForKey:@"repeatAnimation"];
        if ([_animationTypeFromDictionary isEqualToString:@"MOVE"]) {
            self.animationType = MOVE;
        }else{
            self.animationType = ROTATE;
        }
    }
    return self;
}

@end
