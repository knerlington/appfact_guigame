//
//  Entity.h
//  guiGame
//
//  Created by Dan Lakss on 05/10/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Animation.h"
#import "CustomUIImageView.h"
typedef enum{
    SCREEN_END = 0,
    SCREEN_START = 1,
    SCREEN_CUSTOM = 2
}SCREEN_POSITION;

typedef enum{
	RED = 0,
	GREEN = 1
}BG_COLOR;

@interface Entity : NSObject <GestureDelegate>

@property (nonatomic, strong) NSString *image, *name;
@property (nonatomic, assign) CGPoint position;

@property (nonatomic) int posx, posy;
@property (nonatomic,strong) Animation *animation;
@property (nonatomic) SCREEN_POSITION screenPos;
@property (nonatomic) BG_COLOR bgColor;


@property (nonatomic, assign) BOOL hasAnimation;
@property (nonatomic, strong) CustomUIImageView *imageView;

-(instancetype)initWithDictionary:(NSDictionary*)dictionary;
-(void)animateFromIndex:(int)index;

@end
