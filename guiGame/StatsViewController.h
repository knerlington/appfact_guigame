//
//  FightViewController.h
//  guiGame
//
//  Created by Dan Lakss on 28/09/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameEngine.h"

@interface StatsViewController : UIViewController

@property (nonatomic, strong) GameEngine *engine;
@property (nonatomic) int animationIndex;

-(void)animateImageView:(UIImageView*)imageView withCoords:(NSArray*)coords atIndex:(int)index;

@end
