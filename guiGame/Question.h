//
//  Question.h
//  guiGame
//
//  Created by Dan Lakss on 22/09/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 
 Detta objekt används för att hålla ett antal Option-objekt
 
 */

@interface Question : NSObject
@property (nonatomic, strong) NSString *question;
@property (nonatomic, strong) NSNumber *nr, *defaultLink, *timer;
@property (nonatomic, strong) NSMutableArray *options, *flags;


-(instancetype)initWithDictionary:(NSDictionary*)dictionary;
@end
