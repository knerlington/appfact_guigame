//
//  Entity.m
//  guiGame
//
//  Created by Dan Lakss on 05/10/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "Entity.h"



@implementation Entity
-(instancetype)initWithDictionary:(NSDictionary*)dictionary{
    self = [super init];
    if(self){
		
		
        _image = [dictionary objectForKey:@"image"];
        _name = [dictionary objectForKeyedSubscript:@"name"];
        _hasAnimation = [dictionary objectForKey:@"hasAnimation"];
        _imageView = [[CustomUIImageView alloc]init];
		self.imageView.delegate = self;
        _imageView.userInteractionEnabled = YES;
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap)];
//        _imageView.tap = tap;
//        [self.imageView addGestureRecognizer:_imageView.tap];
		
        if([dictionary objectForKey:@"startPosition"]){
            //set position
            NSArray *coords = [dictionary objectForKey:@"startPosition"];
            self.posx = [[[coords objectAtIndex:0]objectForKey:@"x"]doubleValue];
            self.posy = [[[coords objectAtIndex:0]objectForKey:@"y"]doubleValue];
            _position = CGPointMake(self.posx, self.posy);
        }
		//set animations if any
        if(_hasAnimation){
            _animation = [[Animation alloc]initWithDictionary:dictionary];
        }
		//set color props if any
		NSString *colorString = [dictionary objectForKey:@"bgColor"];
		NSString *finalColorString = [colorString uppercaseString];
		if([finalColorString isEqualToString:@"RED"])
			self.bgColor = RED;
		else if([finalColorString isEqualToString:@"GREEN"])
			self.bgColor = GREEN;
		
		UIColor *color = [[UIColor alloc]init];
		switch (self.bgColor) {
			case RED:
				color = [UIColor redColor];
				break;
			case GREEN:
				color = [UIColor greenColor];
			default:
				break;
		}
		self.imageView.backgroundColor = color;
		NSLog(@"Ent color: %@", finalColorString);
    }
    return self;
}
-(void)didTap{
    NSLog(@"Tapped imageView with DELEGATE");
    [self animateFromIndex:0];
}

-(void)animateFromIndex:(int)index{
    NSLog(@"!!!!animateFromIndex: in entity reached!!!!");
    

    if(_hasAnimation){
        NSArray *coords = [NSArray arrayWithArray:self.animation.coordinateList];
        if([[[coords objectAtIndex:index]objectForKey:@"x"]isEqualToString:@"SCREEN_START"]){
            self.screenPos = SCREEN_START;
        }else if ([[[coords objectAtIndex:index]objectForKey:@"x"]isEqualToString:@"SCREEN_END"]){
            self.screenPos = SCREEN_END;
        }else{
            self.screenPos = SCREEN_CUSTOM;
        }
        CGPoint position;
        switch (self.screenPos) {
            case SCREEN_START:
                //start position I.E. 0
                position = CGPointMake(0,[[[coords objectAtIndex:index]objectForKey:@"y"]doubleValue]);
                break;
            case SCREEN_END:
                //end position I.E. view.bounds.width
                position = CGPointMake(self.imageView.superview.bounds.size.width-self.imageView.frame.size.width, [[[coords objectAtIndex:index]objectForKey:@"y"]doubleValue]);
                break;
            case SCREEN_CUSTOM:
                position = CGPointMake([[[coords objectAtIndex:index]objectForKey:@"x"]doubleValue], [[[coords objectAtIndex:index]objectForKey:@"y"]doubleValue]);
                break;
            default:
                break;
        }
        
        
        [UIView animateWithDuration:1.0f
                         animations:^{
                             self.imageView.frame = CGRectMake(position.x, position.y, self.imageView.frame.size.width, self.imageView.frame
                                                               .size.height);
                             NSLog(@"%f", self.imageView.frame.origin.x);
                             
                         }
                         completion:^(BOOL done){
                             if(done){
                                 if(index < [coords count]-1){
                                     __block int newindex = index+1;
                                     [self animateFromIndex:newindex];
                                 }
                                 
                             }
                             

                         }];
    }
}
@end
