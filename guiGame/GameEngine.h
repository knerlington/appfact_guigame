//
//  GameManager.h
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-07.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BeingManager.h"
#import <UIKit/UIKit.h>
#import "Question.h"
#import "Option.h"
#import "Player.h"
#import "Monster.h"
#import "ViewControllerDelegate.h"
#import "FSHelper.h"
#import "Scene.h"
#import "Entity.h"

@interface GameEngine : NSObject <ViewControllerDelegate>

@property (nonatomic, strong) FSHelper *fsHelper;
@property (nonatomic, strong) BeingManager *beingManager; //Manages every being
@property (nonatomic, strong) NSDictionary *actions, *questionsDictionary;
@property (nonatomic, strong) Question *currentQuestion;
@property (nonatomic, strong) Scene *currentScene;
@property (nonatomic) BOOL inFightState;
@property (nonatomic, strong) NSMutableArray *options, *questionsArray, *characterList, *sceneList, *viewList; //content arrays



+(GameEngine*)sharedInstance; //singleton. call this when you want to use the engine

-(void)loadQuestionsFromFileName:(NSString*)fileName;
-(void)loadContentFromFileName:(NSString*)fileName;
-(void)renderQuestionInReceiver:(UITextView*)receiver;
-(void)renderPlayerStatsInReceiver:(id)receiver;
-(int)checkForInputFromSender:(UITextField*)sender;
-(BOOL)loopThroughOptions:(NSArray*)options basedOnInput:(int)input withIndex:(int)index;
-(NSNumber*)checkForLinkInOption:(Option*)option;
-(void)changeQuestionToIndex:(NSNumber*)index;
- (NSDictionary *)dictionaryFromJsonFileWithName:(NSString *)fileName;

-(NSURL*)getImageUrlFromFileName:(NSString*)fileName;


-(void)loadImageForEntitiesInArray:(NSArray*)array;






@end
