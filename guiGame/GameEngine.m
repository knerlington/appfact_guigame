//
//  GameManager.m
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-07.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

//GameManager
//Inits and lets you play the game

#import "GameEngine.h"


#import "Player.h"
#import "Monster.h"


@interface GameEngine()

@property (nonatomic, strong) NSNumber *currentDelayedIndex;

typedef NS_ENUM(NSInteger, actionType){
    newGame = 1,
    loadGame = 2
};

@end


@implementation GameEngine

-(BOOL)shouldSegue{
	return self.inFightState;
}

//---INITIALIZATION---
+(GameEngine*)sharedInstance{
    static GameEngine *instance;
    if(!instance){
        instance = [[self alloc]init];
        instance.questionsArray = [[NSMutableArray alloc]init];
		instance.inFightState = NO;
        instance.fsHelper = [[FSHelper alloc]init];
    }
    return instance;
}

-(void)createNewGame{
    //if user creates new game call this
    //loads a standard player, standard characters, questions etc.
    NSLog(@"!!!!createNewGame reached!!!!");
    [self loadContentFromFileName:@"characters"];

}
-(void)loadGame{
    //if user loads game call this
    //loads stats, questions, characters etc. from a previous game
}

-(void)explore{
    //if user wants to explore call this
    //calls fight: if one should occur
    [self loadContentFromFileName:@"exploreQuestions"];
}

-(void)fight{
    //if monster appears this is called
      [self loadContentFromFileName:@"fightQuestions"];

	
}

-(NSURL*)getImageUrlFromFileName:(NSString *)fileName{
    NSString *path = [self.fsHelper getPathForFileFromBundle:fileName];
    NSURL *url = [NSURL URLWithString:path];
    return url;
}


//---RENDERING---
-(void)renderQuestionInReceiver:(UITextView *)receiver{
    NSLog(@"!!!!renderQuestionInReceiver reached!!!!");
	NSLog(@"%@",self.currentQuestion.question);
	receiver.text = self.currentQuestion.question;
	if(self.currentQuestion.options){
		for(Option *option in self.currentQuestion.options){
			NSLog(@"%@", option.option);
			NSString *optionString = option.option;
			NSString *finalString = [NSString stringWithFormat:@"\r%@", optionString];
			receiver.text = [receiver.text stringByAppendingString:finalString];
		}
	}
	
	
}

-(void)renderPlayerStatsInReceiver:(id)receiver{
    //make sure receiver is of type UITextView
    NSLog(@"!!!!renderPlayerStatsInReceiver reached!!!!");
    if([receiver isKindOfClass:[UITextView class]]){
        for(Player *p in self.characterList){
            if([p isKindOfClass:[Monster class]]){
                
                NSString* finalString = [NSString stringWithFormat:@"Name: %@\rBio: %@\rHealth: %d", p.name, p.bio, p.health];
                ((UITextView*)receiver).text = finalString;
            }
        }
    }
}

//---INPUT---
-(int)checkForInputFromSender:(UITextField *)sender{
    //return input sent from user
    //if input = one of loaded actions perform said action
    return [sender.text intValue];
}
-(BOOL)loopThroughOptions:(NSArray *)options basedOnInput:(int)input withIndex:(int)index{
	//recursive method
	//if input isn't equal to element in options array and input is lower than the nr -
	//of objects keep running 
	if(input-1 == [options indexOfObject:[options objectAtIndex:index]]){
		NSLog(@"User input equals index of option!");
        //check for action in option
        Option *o = [options objectAtIndex:index];
        if(o.action){
            
            // Alternativ
            
            NSArray *actions = @[@"newGame", @"flee"];
            NSInteger index = [actions indexOfObject:o.action];
            switch (index) {
                case 0:
                    NSLog(@"New Game");
                    break;
                case 1:
                    NSLog(@"flee");
                    break;
                default:
                    NSLog(@"No action or action not implemented yet!");
                    break;
            }
            
            // Original
            
            if([o.action isEqual:@"newGame"]){
                //start new game
                [self createNewGame];
            }else if([o.action isEqual:@"loadGame"]){
                //load game
            }else if ([o.action isEqual:@"explore"]){
                //explore
                [self explore];
            }else if([o.action isEqual:@"fight"]){
                //start a fight
				self.inFightState = YES;
                [self fight];
            }else if([o.action isEqual:@"flee"]){
                //flee
                
            }

        }
        if([self checkForLinkInOption:[options objectAtIndex:index]]){
            [self changeQuestionToIndex:[self checkForLinkInOption:[options objectAtIndex:index]]];
            
		}
        

        

       
		return YES;
        
	}else if(input-1 != [options indexOfObject:[options objectAtIndex:index]] && input-1 < [options count]){
		
		[self loopThroughOptions:options basedOnInput:input withIndex:index+1];
	}else{
		NSLog(@"Sorry! User input doesn't match any options for the given question.");
		return NO;
	}
	return nil;
}
-(NSNumber*)checkForLinkInOption:(Option *)option{	//check for a link in passed question options
    if(option.link){
        return option.link;
    }else if(!option.link && option.randomLinks){
        int indexNr = arc4random_uniform((int)[option.randomLinks count]);
        NSNumber *newNr = [option.randomLinks objectAtIndex:indexNr];
//        NSNumber *finalNr = [[NSNumber alloc]initWithInt:nr];
        return newNr;
        
    }else{
        NSLog(@"Sorry the option doesn't have a link.");
    }
    
    return nil;
}

//---CONTENT LOADING---
-(void)loadImageForEntitiesInArray:(NSArray*)array{
    //fetch image for entities based on entity image string name
    NSLog(@"!!!!loadImagesForEntities reached!!!!");
  
    for(Entity *obj in array){
        NSString *tempString = obj.image;
        NSLog(@"%@",tempString);
		if(![obj.image isEqualToString:@""]){
			UIImage *image = [UIImage imageWithContentsOfFile:[self.fsHelper getPathForFileFromBundle:tempString]];
			obj.imageView.image = image;
		}

		
            obj.imageView.frame = CGRectMake(obj.position.x, obj.position.y, 64, 64);
       
        
        
        
    }
}

-(void)loadContentFromFileName:(NSString*)fileName{
    //loads content from json file with passed fileName
    NSLog(@"!!!!loadContentFromFileName: reached!!!!");
    
    NSDictionary *contentDictionary = [self dictionaryFromJsonFileWithName:fileName];
    
    //check type of content
    //questions, characters etc.
    if([contentDictionary objectForKey:@"questions"]){
        //load and create questions
         NSArray *arrayWithQuestions = [contentDictionary objectForKey:@"questions"];

        //loop through the json objects in array and init real question objects
        for(id obj in arrayWithQuestions){
            Question *q = [[Question alloc]initWithDictionary:obj];
            [self.questionsArray addObject:q];
            
            //if json question has options create real options for each real question
            if([obj objectForKey:@"options"]){
                NSLog(@"!!!!options for question found!!!!");
                q.options = [[NSMutableArray alloc]init];
                for(id opt in [obj objectForKey:@"options"]){
                    Option *option = [[Option alloc]initWithDictionary:opt];
                    [q.options addObject:option];
                    
                    
                }
            }
        }
        self.currentQuestion = [self.questionsArray objectAtIndex:0];
    }else if([contentDictionary objectForKey:@"characters"]){
        //load and create characters
        NSArray *tempArray = [contentDictionary objectForKey:@"characters"];
        self.characterList = [[NSMutableArray alloc]init];
        //loop through the json objects in array and init real character objects
        for(id obj in tempArray){
            if([[obj objectForKey:@"type"] isEqual:@"player"]){
                //create player
                Player *p = [[Player alloc]initWithDictionary:obj];
                [_characterList addObject:p];
            }else if([[obj objectForKey:@"type"] isEqual:@"monster"]){
                //create monster
                Monster *m = [[Monster alloc]initWithDictionary:obj];
                [_characterList addObject:m];
            }
        }

    }else if([contentDictionary objectForKey:@"scenes"]){
        NSArray *tempArray = [contentDictionary objectForKey:@"scenes"];
        self.sceneList = [[NSMutableArray alloc]init];
        //loop through the json objects in array and init real scene objects
        for(id obj in tempArray){
            Scene *scn = [[Scene alloc]initWithDictionary:obj];
            [_sceneList addObject:scn];
        }
    }else{
        NSLog(@"ERROR: Dictionary doesn't contain an array with questions or characters!");
    }
    
    //save the json array
   
    
    
    
    
    
    
}

-(void)loadQuestionsFromFileName:(NSString*)fileName{
	//loads and save questions from json file passed as param into a dictionary
	NSLog(@"!!!!loadQuestionsFromFileName: reached!!!!");
	self.questionsDictionary = [self dictionaryFromJsonFileWithName:fileName];
	//save the json array
	NSArray *arrayWithQuestions = [self.questionsDictionary objectForKey:@"questions"];
	self.questionsArray = [[NSMutableArray alloc]init];
	//loop through the json objects in array and init real question objects
	for(id obj in arrayWithQuestions){
		Question *q = [[Question alloc]initWithDictionary:obj];
		[self.questionsArray addObject:q];
		
		//if json question has options create real options for each real question
		if([obj objectForKey:@"options"]){
			NSLog(@"!!!!options for question found!!!!");
			q.options = [[NSMutableArray alloc]init];
			for(id opt in [obj objectForKey:@"options"]){
				Option *option = [[Option alloc]initWithDictionary:opt];
				[q.options addObject:option];
				
				
			}
		}
		//if json question has flags add flags into flags array for the real question
		if([obj objectForKey:@"flags"]){
			NSLog(@"!!!!flags for question found!!!!");
			q.flags = [[NSMutableArray alloc]init];
			for(id flag in [obj objectForKey:@"flags"]){
				[q.flags addObject:flag];
			}
		}
	}
	
	
	self.currentQuestion = [self.questionsArray objectAtIndex:0];
	
}


-(void)changeQuestionToIndex:(NSNumber *)index{
    // Här använder vi en property istället för att skicka med ett värde i en delayed selector ->
//    if (!index  && _currentDelayedIndex) {
//        index = _currentDelayedIndex;
//    }
    NSLog(@"changeQuestion reached!");
	self.currentQuestion = [self.questionsArray objectAtIndex:[index intValue]-1];
    self.currentDelayedIndex = nil;
	
	
    NSLog(@"current question: %@", self.currentQuestion);
}


//---TRADE SECRETS---
- (id)objectFromJsonString:(NSString *)string
{
    if (string)
    {
        NSError *jsonError;
        NSData *jsonData = [string dataUsingEncoding:NSUnicodeStringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&jsonError];
        
        if (jsonError)
            NSLog(@"Error reading json file: %@", jsonError);
        
        return json;
    }
    
    return nil;
}

- (NSDictionary *)dictionaryFromJsonFileWithName:(NSString *)fileName
{
    fileName = [[fileName lowercaseString] hasSuffix:@".json"] ? [fileName substringToIndex:(fileName.length - 4)] : fileName;
    
    NSError *stringError;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    
    if (filePath)
    {
        NSString *jsonString = [NSString stringWithContentsOfFile:filePath
                                                         encoding:NSUTF8StringEncoding
                                                            error:&stringError];
        if (jsonString)
            return [self objectFromJsonString:jsonString];
    }
    
    return nil;
}




@end
