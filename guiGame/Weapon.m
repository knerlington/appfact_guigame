//
//  Weapon.m
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-07.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "Weapon.h"

@implementation Weapon
-(instancetype)initWithName:(NSString *)name andDamage:(int)damage{
    self = [super init];
    if(self){
        self.name = name;
        self.damage = damage;
        NSLog(@"Weapon created!");
    }
    return self;
}

@end
