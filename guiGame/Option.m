//
//  Option.m
//  guiGame
//
//  Created by Dan Lakss on 22/09/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "Option.h"

@implementation Option
-(instancetype)initWithDictionary:(NSDictionary*)dictionary{
	self = [super init];
	
	if(self){
        self.action = [dictionary objectForKey:@"action"];
		self.option = [dictionary objectForKey:@"option"];
		if([dictionary objectForKey:@"link"]){
			self.link = [dictionary objectForKey:@"link"];
		}
        if([dictionary objectForKey:@"randomLink"]){
            
            NSArray *tempArray = [NSArray arrayWithArray:[dictionary objectForKey:@"randomLink"]];
            _randomLinks = [[NSArray alloc]initWithArray:[tempArray valueForKey:@"nr"]];
        }

	}
	return self;
}
@end
