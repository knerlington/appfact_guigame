//
//  Player.m
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-07.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "Player.h"

@implementation Player

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super initWithDictionary:dictionary];
    if (self) {
        
        // Player
        
        self.name = [dictionary objectForKey:@"name"];
        self.health = [[dictionary objectForKey:@"health"] intValue];
        // etc.
        
        // Weapons
        
        NSArray *weapons = [dictionary objectForKey:@"weapons"];
        if (weapons) {
            NSMutableArray *tmp = [NSMutableArray new];
            for (NSDictionary *weaponData in weapons) {
                NSString *weaponName = [weaponData objectForKey:@"name"]; // Longsword
                NSString *weaponType = [weaponData objectForKey:@"type"]; // Sword
                Weapon *weapon = [[Weapon alloc] initWithName:weaponName andDamage:10]; // TODO - Add "Damage" to plist!
                weapon.type = weaponType;
                [tmp addObject:weapon];
            }
            self.weapons = tmp;
        } else {
            NSLog(@"Didn't find weapon data");
        }
    }
    return self;
}

-(void)printPlayerData{
    NSLog(@"-printPlayerData reached!");
    NSLog(@"Nr of weapons: %lu", (unsigned long)[self.weapons count]);
}
-(void)changeWeapon{
    
    //loop through each weapon in self.weapons
    //set currentWeapong = weapon of choice
    for(NSDictionary *dict in self.weapons){
        NSLog(@"%@", dict);
    }
    
}


@end
