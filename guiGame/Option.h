//
//  Option.h
//  guiGame
//
//  Created by Dan Lakss on 22/09/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 
 Detta objekt används för att representera ett alternativ
 
 */

@interface Option : NSObject
@property (nonatomic, strong) NSString *option;
@property (nonatomic, strong) NSNumber *link;
@property (nonatomic,strong) NSArray *randomLinks;
@property (nonatomic, strong) NSString *action;

-(instancetype)initWithDictionary:(NSDictionary*)dictionary;
@end
