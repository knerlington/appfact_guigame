//
//  SceneViewController.h
//  guiGame
//
//  Created by Dan Lakss on 05/10/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Scene.h"
#import "Entity.h"

@interface SceneViewController : UIViewController
@property (nonatomic, strong) Scene *currentScene;
@end
