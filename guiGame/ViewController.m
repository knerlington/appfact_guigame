//
//  ViewController.m
//  guiGame
//
//  Created by Dan Lakss on 16/09/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *label_title;
@property (nonatomic, strong) NSMutableArray *buttonList;

@end

@implementation ViewController




- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //---INITIALIZATION---
	_buttonList = [[NSMutableArray alloc]init];
    //FSHelper
    self.fsHelper = [[FSHelper alloc]init];
	//GameEngine
//    self.gm = [[GameEngine alloc]initWithQuestionsFromFileName:@"questions2"];
    self.gm = [GameEngine sharedInstance];
	self.delegate = self.gm;
	//[self.gm loadQuestionsFromFileName:@"questions2"];
    [self.gm loadContentFromFileName:@"initQuestions"];
 
    //[self.gm actOnFlagsForQuestion:self.gm.currentQuestion];
	[self.gm renderQuestionInReceiver:self.textView_output];
	//[self.gm checkForFlagsInQuestion:self.gm.currentQuestion];
	[self createButtonsFromOptions];


	
}

-(void)startFightScene{
    NSLog(@"Fight triggered!");

}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
}

-(void)createButtonsFromOptions{
    CGFloat posX = 10, posY = 100;
    for(UIButton *btn in _buttonList){
        [btn removeFromSuperview];
    }
    [_buttonList removeAllObjects];
    
    NSInteger i = 0;
	for(Option *opt in self.gm.currentQuestion.options){
		UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [btn addTarget:self action:@selector(buttonEvent:) forControlEvents:UIControlEventTouchUpInside];
        NSString *title = [NSString stringWithFormat:@"Option %i", (int)[self.gm.currentQuestion.options indexOfObject:opt]+1];
        [btn setTitle:title forState:UIControlStateNormal];
        CGSize labelSize = [btn.titleLabel sizeThatFits:self.view.bounds.size];
        CGFloat margin = 8;
        CGRect rect = CGRectMake(posX, posY, labelSize.width+2*margin, labelSize.height+2*margin);
        btn.frame = rect;
		[_buttonList addObject:btn];
		posX += btn.frame.size.width+margin;
        btn.tag = i;
        i++;
       
	}
    [self renderButtons];
//	for(UIButton *obj in _buttonList){
//		
//		[self.view addSubview:obj];
//		
//		
//
//	}
}
-(void)renderButtons{
    for(UIButton *btn in _buttonList){
        [self.view addSubview:btn];
    }
}

-(void)buttonEvent:(id)sender{
	NSLog(@"Programmatically created button was pressed!");
    
    	[NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self.gm loopThroughOptions:self.gm.currentQuestion.options basedOnInput:(int)((UIButton*)sender).tag+1 withIndex:0];
    [self.gm renderQuestionInReceiver:self.textView_output];
    
        [self createButtonsFromOptions];
    //if timer perform selector when it ends
    if(self.gm.currentQuestion.timer){
        [self performSelector:@selector(timerEvent) withObject:nil afterDelay:[self.gm.currentQuestion.timer intValue]];
    }
    //check if fight is triggered
	if([self.delegate shouldSegue]){
		NSLog(@"Engine triggered a fight as a delegate to the view!");
        //StatsViewController *svc = [[StatsViewController alloc] init];
        StatsViewController *svc = (StatsViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"statsStoryboardId"];
        //[self.navigationController presentViewController:svc animated:YES completion:nil];
        [self.navigationController pushViewController:svc animated:YES];
        //[self performSegueWithIdentifier:@"gotoStatsSegue" sender:nil];
	}
		
}

-(void)timerEvent{
	//callback for when the question timer runs out
	UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Timer alert" message:@"The timer ran out!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	[self.gm changeQuestionToIndex:self.gm.currentQuestion.defaultLink];
	[self.gm renderQuestionInReceiver:self.textView_output];
}
-(void)enterInput{
	//cancel previous selector call if there is one
	[NSObject cancelPreviousPerformRequestsWithTarget:self];
    NSString *input = self.textField_input.text;
    
    self.textView_output.text = [self.textView_output.text stringByAppendingString:[NSString stringWithFormat:@"\r%@%@",@">Input: " ,input]];
    NSLog(@"intValue of input: %d",[self.gm checkForInputFromSender:self.textField_input]);

	
	[self.gm loopThroughOptions:self.gm.currentQuestion.options basedOnInput:[self.gm checkForInputFromSender:self.textField_input] withIndex:0];
	

	[self.gm renderQuestionInReceiver:self.textView_output];
	
	//if timer perform selector when it ends
	if(self.gm.currentQuestion.timer){
		[self performSelector:@selector(timerEvent) withObject:nil afterDelay:[self.gm.currentQuestion.timer intValue]];
	}
	
		
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(![self.textField_input.text isEqualToString:@""]){
        [self enterInput];
		
    }
    
    
    [self.textField_input resignFirstResponder];
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
