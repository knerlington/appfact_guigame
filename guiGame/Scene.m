//
//  Scene.m
//  guiGame
//
//  Created by Dan Lakss on 05/10/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "Scene.h"
@interface Scene ()

@end

@implementation Scene

-(instancetype)initWithDictionary:(NSDictionary*)dictionary{
    self = [super init];
    
    if(self){
        _entityList = [[NSMutableArray alloc]init];
        _counter = [dictionary objectForKey:@"counter"];
        _bgColor = [dictionary objectForKey:@"bgColor"];
        _name = [dictionary objectForKey:@"name"];
        if([dictionary objectForKey:@"entities"]){
            NSArray *tempArray = [dictionary objectForKey:@"entities"];
            for(id obj in tempArray){
                Entity *ent = [[Entity alloc]initWithDictionary:obj];
                [_entityList addObject:ent];
            }
        }
            
    }
    
    return self;
}
@end
