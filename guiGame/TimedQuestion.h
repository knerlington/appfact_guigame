//
//  TimedQuestion.h
//  guiGame
//
//  Created by Dan Lakss on 2015-09-23.
//  Copyright © 2015 Dan Lakss. All rights reserved.
//

#import "Question.h"

@interface TimedQuestion : Question
@property (nonatomic, strong) NSNumber *defaultLink;
@property (nonatomic, strong) NSArray *flags;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
@end
