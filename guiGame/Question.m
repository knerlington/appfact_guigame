//
//  Question.m
//  guiGame
//
//  Created by Dan Lakss on 22/09/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "Question.h"

@implementation Question

-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
	self = [super init];
	if(self){
		self.question = [dictionary objectForKey:@"question"];
		self.nr = [dictionary objectForKey:@"nr"];
		if([dictionary objectForKey:@"defaultLink"]){
			_defaultLink = [dictionary objectForKey:@"defaultLink"];
		}
		if([dictionary objectForKey:@"timer"]){
			_timer = [dictionary objectForKey:@"timer"];
			
		}
	}
	return self;
}

@end
