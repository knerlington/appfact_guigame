//
//  SceneViewController.m
//  guiGame
//
//  Created by Dan Lakss on 05/10/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "SceneViewController.h"
#import "GameEngine.h"
#import "Scene.h"
#import "Entity.h"

@interface SceneViewController ()
@property (nonatomic, strong) GameEngine *engine;
@property (nonatomic, strong) NSMutableArray *viewList;
-(void)animateViews;
@end

@implementation SceneViewController


//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
//    for(Entity *obj in self.currentScene.entityList){
//        [obj touchesBegan:touches withEvent:event];
//    }
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _viewList = [[NSMutableArray alloc]init];
    self.engine = [GameEngine sharedInstance];
    [self.engine loadContentFromFileName:@"scenes"];
    _currentScene = [self.engine.sceneList objectAtIndex:0];
    for(id obj in self.engine.sceneList){
        NSLog(@"%@",((Scene*)obj).name);
    }
    
    [self loadImagesForEntities];
    [self renderViews];
    //[self animateViews];
  
    

    // Do any additional setup after loading the view.
}

-(void)loadImagesForEntities{
    NSLog(@"!!!!loadImagesForEntities reached!!!!");

    [self.engine loadImageForEntitiesInArray:_currentScene.entityList];
    for(Entity *obj in _currentScene.entityList){
//        CGPoint position = CGPointMake(0, self.view.bounds.size.height/2);
//        obj.imageView.frame = CGRectMake(position.x, position.y-obj.imageView.image.size.height/2, obj.imageView.image.size.width, obj.imageView.image.size.height);
        [_viewList addObject:obj.imageView];

    }
    
}
-(void)animateViews{
    NSLog(@"!!!!animateViews in scene controller reached!!!!");
    for(Entity *ent in self.currentScene.entityList){
        [ent animateFromIndex:0];
       
    }
  
}
-(void)renderViews{
        NSLog(@"!!!!renderViews reached!!!!");
    for(id obj in _viewList){
        [self.view addSubview:obj];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
