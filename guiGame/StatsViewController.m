//
//  FightViewController.m
//  guiGame
//
//  Created by Dan Lakss on 28/09/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "StatsViewController.h"

@interface StatsViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView_output;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_enemy;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_player;

@end

@implementation StatsViewController

-(void)animateImageView:(UIImageView*)imageView withCoords:(NSArray *)coords atIndex:(int)index{
    CGFloat sizeX, sizeY;

    
    CGFloat x = [[[coords objectAtIndex:index]valueForKey:@"x"]doubleValue];
    CGFloat y = [[[coords objectAtIndex:index]objectForKey:@"y"]doubleValue];
    CGPoint position = CGPointMake(x, y);
    sizeX = imageView.frame.size.width;
    sizeY = imageView.frame.size.height;
    NSLog(@"%@x: %f", imageView.image,imageView.frame.origin.x);
        NSLog(@"%@y: %f", imageView.image, imageView.frame.origin.y);
    
   
    
    [UIView animateWithDuration:1.0f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn // UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         imageView.frame = CGRectMake(position.x, position.y, sizeX,sizeY);
        
    }completion:^(BOOL done){
        if(index < [coords count]-1){
            __block int newindex = index+1;
            
            [self animateImageView:imageView withCoords:coords atIndex:newindex];
        }
        
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //GameEngine init
    self.engine = [GameEngine sharedInstance];
    _animationIndex = 0;
    //content loaded from engine based on option action
    [self.engine renderPlayerStatsInReceiver:self.textView_output];
    
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    //get images from url for characters
    NSLog(@"Nr of characters: %lu",(unsigned long)[self.engine.characterList count]);
    for(int i = 0; i < [self.engine.characterList count];i++){
        Being *b = [self.engine.characterList objectAtIndex:i];
      
        UIImage *image = [UIImage imageWithContentsOfFile:[self.engine.fsHelper getPathForFileFromBundle:b.image]];
        
        switch (i) {
            case 0:
                _imageView_player.image = image;
                _imageView_player.frame = CGRectMake(0, 0, _imageView_player.frame.size.width,_imageView_player.frame.size.height);
                [self animateImageView:_imageView_player withCoords:b.coords atIndex:0];

                break;
            case 1:
                _imageView_enemy.image = image;
                _imageView_enemy.frame = CGRectMake(0, 0, _imageView_enemy.frame.size.width,_imageView_enemy.frame.size.height);
                [self animateImageView:_imageView_enemy withCoords:b.coords atIndex:0];
                break;
            default:
                break;
        }

        
    }
//    NSData *imageData = [[NSData alloc] initWithContentsOfURL:imageURL];
//    UIImage *image = [[UIImage alloc] initWithData:imageData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
