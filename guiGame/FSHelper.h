//
//  FSHelper.h
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-09.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FSHelper : NSObject
@property (nonatomic, strong) NSFileManager *fm;

-(NSArray*)loadContentFromFileWithName:(NSString*)fileName;//DEPRECATED
-(void)saveContentToDictionary;//DEPRECATED

-(id)init;
-(NSString*)getPathForFileFromBundle:(NSString*)file;
-(NSURL*)getDocumentsPath;

-(void)saveToFileName:(NSString*)fileName withData:(id)object;
-(NSString*)loadFromFileName:(NSString*)fileName;




@end
