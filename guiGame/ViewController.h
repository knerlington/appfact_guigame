//
//  ViewController.h
//  guiGame
//
//  Created by Dan Lakss on 16/09/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSHelper.h"
#import "GameEngine.h"
#import "StatsViewController.h"
#import "ViewControllerDelegate.h"



@interface ViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextView *textView_output;
@property (weak, nonatomic) IBOutlet UITextField *textField_input;
@property (weak, nonatomic) IBOutlet UIButton *button_input;
@property (nonatomic) SEL timerCallback;
@property (nonatomic, weak) id<ViewControllerDelegate> delegate;


//FSHelper - File system
@property (nonatomic, strong) FSHelper *fsHelper;

//GameManager - Game logic
@property (nonatomic, strong) GameEngine *gm;

-(void)timerEvent;

@end

