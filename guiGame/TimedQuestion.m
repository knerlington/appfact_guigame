//
//  TimedQuestion.m
//  guiGame
//
//  Created by Dan Lakss on 2015-09-23.
//  Copyright © 2015 Dan Lakss. All rights reserved.
//

#import "TimedQuestion.h"

@implementation TimedQuestion
-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
	self = [super initWithDictionary:dictionary];
	self.flags = [dictionary objectForKey:@"flags"];
	self.defaultLink = [dictionary objectForKey:@"defaultLink"];
	return self;
}
@end
