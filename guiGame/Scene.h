//
//  Scene.h
//  guiGame
//
//  Created by Dan Lakss on 05/10/15.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Entity.h"

@interface Scene : NSObject
@property (nonatomic, strong) NSMutableArray *entityList;
@property (nonatomic, strong) NSNumber *counter;
@property (nonatomic, strong) NSMutableString *bgColor, *name;
-(instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
