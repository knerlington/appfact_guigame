//
//  BeingManager.m
//  appFactCommandLine
//
//  Created by Dan Lakss on 2015-09-09.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "BeingManager.h"


@implementation BeingManager

//sharedInstance - Class method returning an instance of the class
//call this whenever you want to handle a being
+(BeingManager*)sharedInstance{
    static BeingManager *instance;
    
    if(!instance){
        instance = [[self alloc]init];
        instance.beings = [[NSMutableArray alloc]init];
        instance.fsHelper = [[FSHelper alloc]init];
        
    }
    return instance;
}
-(id)createBeing:(id)type withHealth:(int)health andName:(NSString*)name andStrength:(int)strength{
    type = [[type alloc]initWith:health andName:name andStrength:strength];
    [self.beings addObject:type];
    return type;
}

-(void)createLoadedContent:(NSArray*)array{
//    
//    NSArray *monsters = [dictionary objectForKey:@"Monsters"];
//    NSMutableArray *tmp = [NSMutableArray new];
//    
//    for (NSDictionary *monsterData in monsters) {
//        NSString *monsterName = [monsterData objectForKey:@"monsterName"];
//        Monster *monster = [[Monster alloc] initWithName:monsterName];
//        [tmp addObject:monster];
//    }
//    
//    NSDictionary *playerData = [dictionary objectForKey:@"Player"];
//    Player *player = [[Player alloc] initWithDictionary:playerData];
//    
//    [tmp addObject:player]; // For collision purposes etc.
//    
//    self.beings = tmp;
    
    
    
    //content fetched is used to instantiate corresponding objects
    NSLog(@"createLoadeadContent: reached!");
    for(NSDictionary *dict in array){
        //check type of each dict obj
        NSString *type = [dict objectForKey:@"type"];
        //save all keys for each obj
        NSArray *keyArray = [dict allKeys];
        
        //check if type equals player or monster
        if([type isEqualToString:@"player"]){
            NSLog(@"Player object found!");
//            Player *p = [[Player alloc] initWithDictionary:dict];
//
            Player *p = [Player new];
            
            for(NSString *key in keyArray){
                
                if ([key isEqualToString:@"weapons"]) {
                    
                } else {
                    [p setValue:[dict objectForKey:key] forKey:key];
                }
                
                
                
            }
            
            //add the player to list of beings
            [self.beings addObject:p];
            NSLog(@"All values from dictionary object with type 'player' set to player object!");
            self.player = p;
            
                
        }else if([type isEqualToString:@"monster"]){
            Monster *m = [Monster new];
            for(NSString *key in keyArray){
                [m setValue:[dict objectForKey:key] forKey:key];
            }
            [self.beings addObject:m];
            NSLog(@"All values from dict object with type 'monster' set to monster object");
        }
        
        
        
    }
    
}

-(void)viewBeingInfo:(NSString *)name{
    //fetch being info from param
    for(Being *b in self.beings){
        if([b.name isEqualToString:name]){
            NSLog(@"----INFO FOR TYPE: %@ WITH NAME: %@----", b.type, b.name);
            NSLog(@"Type: %@", b.type);
            NSLog(@"Name: %@", b.name);
            NSLog(@"Health: %i", b.health);
            NSLog(@"Strength: %i", b.strength);
            
        }
    }
    
}
-(void)kill:(Being*)being{
    //Kill entered being from beings[]
}

@end
